package com.gildedrose;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class DegradingQuality implements ProgressStrategy {

    private final static int DEFAULT_DEGRADATION_FACTOR = 1;
    private final int degradationFactor;

    public final static int TWICE_AS_FAST = 2;

    public DegradingQuality() {
        this.degradationFactor = DEFAULT_DEGRADATION_FACTOR;
    }

    public DegradingQuality(int degradationFactor) {
        this.degradationFactor = degradationFactor;
    }

    @Override
    public DecoratedItem progress(DecoratedItem item) {
        decreaseQuality(item);
        item.progressSellIn();

        if (item.sellByDateHasPassed()) {
            decreaseQuality(item);
        }

        return item;
    }

    private void decreaseQuality(DecoratedItem item) {
        for (int i = 0; i < degradationFactor; i++) {
            item.decreaseQuality();
        }
    }
}
