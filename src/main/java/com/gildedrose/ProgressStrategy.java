package com.gildedrose;

public interface ProgressStrategy {
    DecoratedItem progress(DecoratedItem item);
}
