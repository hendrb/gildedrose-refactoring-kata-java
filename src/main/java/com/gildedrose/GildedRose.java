package com.gildedrose;

import java.util.Arrays;
import java.util.List;

class GildedRose {
    Item[] items;

    private final ProgressStrategyFactory progressRuleFactory = new ProgressStrategyFactory();

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public GildedRose(List<Item> items) {
        this.items = items.toArray(new Item[0]);
    }

    public void updateQuality() {
        this.items = Arrays.stream(items)
            .map(this::progress)
            .toArray(Item[]::new);
    }

    private Item progress(Item item) {
        return decorated(item)
            .progressWith(progressStrategyFor(item))
            .asItem();
    }

    private ProgressStrategy progressStrategyFor(Item item) {
        return progressRuleFactory.progressStrategyFor(item);
    }

    private DecoratedItem decorated(Item item) {
        return new DecoratedItem(item);
    }
}
