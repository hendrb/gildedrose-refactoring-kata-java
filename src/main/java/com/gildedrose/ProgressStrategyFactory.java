package com.gildedrose;

import java.util.Map;

import static com.gildedrose.DegradingQuality.TWICE_AS_FAST;
import static com.gildedrose.Items.*;

public final class ProgressStrategyFactory {

    private static final ProgressStrategy DEFAULT_PROGRESS_STRATEGY = new DegradingQuality();

    private final Map<Items, ProgressStrategy> specializedStrategies;

    public ProgressStrategyFactory() {
        this.specializedStrategies =
            Map.of(
                AGED_BRIE, new AgeIncreasesQuality(),
                SULFURAS, new LegendaryItem(),
                BACKSTAGE_PASSES, new BackstagePasses(),
                CONJURED_MANA_CAKE, new DegradingQuality(TWICE_AS_FAST)
            );
    }

    public ProgressStrategy progressStrategyFor(Item item) {
        return Items.from(item)
            .map(this::toSpecializedProgressStrategy)
            .orElse(defaultProgressRule());
    }

    private static ProgressStrategy defaultProgressRule() {
        return DEFAULT_PROGRESS_STRATEGY;
    }

    private ProgressStrategy toSpecializedProgressStrategy(Items item) {
        return this.specializedStrategies.getOrDefault(item, defaultProgressRule());
    }
}
