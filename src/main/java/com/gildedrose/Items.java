package com.gildedrose;

import java.util.Arrays;
import java.util.Optional;

public enum Items {
    AGED_BRIE("Aged Brie"),
    SULFURAS("Sulfuras, Hand of Ragnaros"),
    BACKSTAGE_PASSES("Backstage passes to a TAFKAL80ETC concert"),
    DEXTERITY_VEST("+5 Dexterity Vest"),
    ELIXIR_OF_THE_MONGOOSE("Elixir of the Mongoose"),
    CONJURED_MANA_CAKE("Conjured Mana Cake");

    private final String name;

    Items(String name) {
        this.name = name;
    }

    public static Optional<Items> from(Item item) {
        return Arrays.stream(Items.values())
            .filter(items -> items.name.equals(item.name))
            .findFirst();
    }

    public String asName() {
        return name;
    }
}
