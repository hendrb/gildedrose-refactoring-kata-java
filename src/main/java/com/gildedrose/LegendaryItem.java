package com.gildedrose;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class LegendaryItem implements ProgressStrategy {

    @Override
    public DecoratedItem progress(DecoratedItem item) {
        return legendaryItemsNeverHaveToBeSoldOrDecreasedInQuality(item);
    }

    private DecoratedItem legendaryItemsNeverHaveToBeSoldOrDecreasedInQuality(DecoratedItem item) {
        return item;
    }
}
