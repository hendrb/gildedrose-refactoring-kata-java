package com.gildedrose;

import java.util.Objects;
import java.util.function.Predicate;

public class DecoratedItem {
    private static final int MAXIMUM_QUALITY = 50;
    private static final int MINIMUM_QUALITY = 0;

    public static final Predicate<DecoratedItem> TEN_DAYS_OR_LESS = item -> item.asItem().sellIn < 11;
    public static final Predicate<DecoratedItem> FIVE_DAYS_OR_LESS = item -> item.asItem().sellIn < 6;

    private Item item;

    public DecoratedItem(Item item) {
        this.item = new Item(item.name, item.sellIn, item.quality);
    }

    public DecoratedItem progressWith(ProgressStrategy progressStrategy) {
        var progressed = progressStrategy.progress(this);
        this.item = progressed.asItem();
        return this;
    }

    public boolean sellInFiveDaysOrLess() {
        return sellIn(FIVE_DAYS_OR_LESS);
    }

    public boolean sellInTenDaysOrLess() {
        return sellIn(TEN_DAYS_OR_LESS);
    }

    private boolean sellIn(Predicate<DecoratedItem> predicate) {
        return predicate.test(this);
    }

    public void decreaseQuality() {
        if (item.quality > MINIMUM_QUALITY) {
            item.quality = item.quality - 1;
        }
    }

    public void increaseQuality() {
        if (item.quality < MAXIMUM_QUALITY) {
            item.quality = item.quality + 1;
        }
    }

    public boolean sellByDateHasPassed() {
        return item.sellIn < 0;
    }

    public void progressSellIn() {
        item.sellIn = item.sellIn - 1;
    }

    public void reduceToMinimumQuality() {
        item.quality = MINIMUM_QUALITY;
    }

    public Item asItem() {
        return item;
    }

    public int quality() {
        return item.quality;
    }

    public int sellIn() {
        return item.sellIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DecoratedItem that = (DecoratedItem) o;
        return Objects.equals(item.name, that.item.name)
            && Objects.equals(item.quality, that.item.quality)
            && Objects.equals(item.sellIn, that.item.sellIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item.name, item.quality, item.sellIn);
    }
}
