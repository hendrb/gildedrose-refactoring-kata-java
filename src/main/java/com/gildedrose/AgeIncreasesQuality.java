package com.gildedrose;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class AgeIncreasesQuality implements ProgressStrategy {

    @Override
    public DecoratedItem progress(DecoratedItem item) {
        item.increaseQuality();
        item.progressSellIn();

        if (item.sellByDateHasPassed()) {
            item.increaseQuality();
        }

        return item;
    }
}
