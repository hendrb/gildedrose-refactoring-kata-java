package com.gildedrose;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class BackstagePasses implements ProgressStrategy {

    @Override
    public DecoratedItem progress(DecoratedItem item) {
        item.increaseQuality();

        if (item.sellInTenDaysOrLess()) {
            item.increaseQuality();
        }

        if (item.sellInFiveDaysOrLess()) {
            item.increaseQuality();
        }

        item.progressSellIn();

        if (item.sellByDateHasPassed()) {
            item.reduceToMinimumQuality();
        }

        return item;
    }
}
