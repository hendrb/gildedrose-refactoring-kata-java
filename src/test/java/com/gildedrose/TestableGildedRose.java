package com.gildedrose;

import java.util.Arrays;
import java.util.List;

public class TestableGildedRose {
    private final GildedRose gildedRose;

    public TestableGildedRose(List<Item> items) {
        this.gildedRose = new GildedRose(items);
    }

    public TestableGildedRose progressOneDay() {
        gildedRose.updateQuality();
        return this;
    }

    public TestableGildedRose progressDays(int days) {
        for (int i = 0; i < days; i++) {
            progressOneDay();
        }
        return this;
    }

    public List<Item> items() {
        return Arrays.asList(gildedRose.items);
    }

    @Override
    public String toString() {
        return items().toString();
    }
}
