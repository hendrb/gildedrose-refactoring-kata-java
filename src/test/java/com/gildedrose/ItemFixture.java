package com.gildedrose;

import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Accessors(chain = true, fluent = true)
class ItemFixture {

    public static final String ITEM_NAME = "Some Item";
    public static final int ITEM_QUALITY = 10;
    public static final int SELL_IN = 20;

    String name = ITEM_NAME;
    int sellIn = ITEM_QUALITY;
    int quality = SELL_IN;

    public static ItemFixture anItem() {
        return new ItemFixture();
    }

    public Item build() {
        return new Item(name, sellIn, quality);
    }

    public ItemFixture name(Items itemName) {
        this.name = itemName.asName();
        return this;
    }

    public ItemFixture name(String value) {
        this.name = value;
        return this;
    }
}
