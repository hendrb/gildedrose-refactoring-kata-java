package com.gildedrose;

import org.assertj.core.api.Condition;

import static com.gildedrose.ItemPredicates.*;

public final class ItemConditions {
    private ItemConditions() {
    }

    public static Condition<Item> item(Item item) {
        return new Condition<>(ItemPredicates.item(item), "Item should match " + item);
    }

    public static Condition<Item> withQuality(int quality) {
        return new Condition<>(quality(quality), "Item quality should match " + quality);
    }

    public static Condition<Item> withSellIn(int sellIn) {
        return new Condition<>(sellIn(sellIn), "Item sellIn should match " + sellIn);
    }

    public static Condition<Item> withName(String name) {
        return new Condition<>(name(name), "Item name should match " + name);
    }
}
