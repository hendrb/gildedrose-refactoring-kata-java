package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.DegradingQuality.TWICE_AS_FAST;
import static com.gildedrose.ItemFixture.anItem;
import static org.assertj.core.api.Assertions.assertThat;

class DegradingQualityTest {

    @Test
    void normalDegradation() {
        var degradation = new DegradingQuality();

        var actual = degradation.progress(new DecoratedItem(anItem().quality(1).sellIn(1).build()));

        assertThat(actual.quality()).isEqualTo(0);
        assertThat(actual.sellIn()).isEqualTo(0);
    }

    @Test
    void twiceAsFastDegradation() {
        var degradation = new DegradingQuality(TWICE_AS_FAST);

        var actual = degradation.progress(new DecoratedItem(anItem().quality(2).sellIn(1).build()));

        assertThat(actual.quality()).isEqualTo(0);
        assertThat(actual.sellIn()).isEqualTo(0);
    }

    @Test
    void twiceAsFastDegradation_sellInReached() {
        var degradation = new DegradingQuality(TWICE_AS_FAST);

        var actual = degradation.progress(new DecoratedItem(anItem().quality(4).sellIn(0).build()));

        assertThat(actual.quality()).isEqualTo(0);
        assertThat(actual.sellIn()).isEqualTo(-1);
    }

    @Test
    void twiceAsFastDegradation_sellInReached_bottom() {
        var degradation = new DegradingQuality(TWICE_AS_FAST);

        var actual = degradation.progress(new DecoratedItem(anItem().quality(3).sellIn(0).build()));

        assertThat(actual.quality()).isEqualTo(0);
        assertThat(actual.sellIn()).isEqualTo(-1);
    }

    @Test
    void twiceAsFastDegradation_bottom() {
        var degradation = new DegradingQuality(TWICE_AS_FAST);

        var actual = degradation.progress(new DecoratedItem(anItem().quality(1).sellIn(1).build()));

        assertThat(actual.quality()).isEqualTo(0);
        assertThat(actual.sellIn()).isEqualTo(0);
    }
}
