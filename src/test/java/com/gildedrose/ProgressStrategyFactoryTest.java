package com.gildedrose;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.gildedrose.DegradingQuality.TWICE_AS_FAST;
import static com.gildedrose.ItemFixture.anItem;
import static com.gildedrose.Items.*;
import static org.assertj.core.api.Assertions.assertThat;

class ProgressStrategyFactoryTest {

    private ProgressStrategyFactory progressStrategyFactory;

    @BeforeEach
    void setUp() {
        progressStrategyFactory = new ProgressStrategyFactory();
    }

    @Test
    void defaultStrategy() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name("it still works").build());

        assertThat(actual).isEqualTo(new DegradingQuality());
    }

    @Test
    void withoutSpecializedStrategy() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name(ELIXIR_OF_THE_MONGOOSE).build());

        assertThat(actual).isEqualTo(new DegradingQuality());
    }

    @Test
    void ageIncreasesQuality() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name(AGED_BRIE).build());

        assertThat(actual).isEqualTo(new AgeIncreasesQuality());
    }

    @Test
    void legendary() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name(SULFURAS).build());

        assertThat(actual).isEqualTo(new LegendaryItem());
    }

    @Test
    void backstagePasses() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name(BACKSTAGE_PASSES).build());

        assertThat(actual).isEqualTo(new BackstagePasses());
    }

    @Test
    void conjured() {
        var actual = progressStrategyFactory.progressStrategyFor(anItem().name(CONJURED_MANA_CAKE).build());

        assertThat(actual).isEqualTo(new DegradingQuality(TWICE_AS_FAST));
    }
}
