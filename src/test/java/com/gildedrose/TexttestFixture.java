package com.gildedrose;

import static com.gildedrose.Items.*;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("OMGHAI!");

        Item[] items = new Item[]{
            new Item(DEXTERITY_VEST.asName(), 10, 20), //
            new Item(AGED_BRIE.asName(), 2, 0), //
            new Item(ELIXIR_OF_THE_MONGOOSE.asName(), 5, 7), //
            new Item(SULFURAS.asName(), 0, 80), //
            new Item(SULFURAS.asName(), -1, 80),
            new Item(BACKSTAGE_PASSES.asName(), 15, 20),
            new Item(BACKSTAGE_PASSES.asName(), 10, 49),
            new Item(BACKSTAGE_PASSES.asName(), 5, 49),
            new Item(CONJURED_MANA_CAKE.asName(), 3, 6),
            new Item(CONJURED_MANA_CAKE.asName(), 3, 12)};

        GildedRose app = new GildedRose(items);

        int days = 8;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : app.items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }

}
