package com.gildedrose;

import java.util.function.Predicate;

public final class ItemPredicates {
    private ItemPredicates() {
    }

    public static Predicate<Item> item(Item item) {
        return name(item.name).and(sellIn(item.sellIn)).and(quality(item.quality));
    }

    public static Predicate<Item> name(String name) {
        return item -> item.name.equals(name);
    }

    public static Predicate<Item> sellIn(int sellIn) {
        return item -> item.sellIn == sellIn;
    }

    public static Predicate<Item> quality(int quality) {
        return item -> item.quality == quality;
    }
}
