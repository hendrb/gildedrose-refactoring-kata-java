package com.gildedrose;

import org.junit.jupiter.api.Test;

import static com.gildedrose.ItemConditions.*;
import static com.gildedrose.ItemFixture.*;
import static org.assertj.core.api.Assertions.assertThat;

class DecoratedItemTest {

    @Test
    void decoratedItemWithEquals() {
        assertThat(new DecoratedItem(anItem().build()))
            .isEqualTo(new DecoratedItem(anItem().build()));
    }

    @Test
    void decreaseQuality() {
        var item = new DecoratedItem(anItem().quality(10).build());

        item.decreaseQuality();

        assertThat(item.quality()).isEqualTo(9);
    }

    @Test
    void decreaseQuality_bottomed() {
        var item = new DecoratedItem(anItem().quality(0).build());

        item.decreaseQuality();

        assertThat(item.quality()).isEqualTo(0);
    }

    @Test
    void increaseQuality() {
        var item = new DecoratedItem(anItem().quality(10).build());

        item.increaseQuality();

        assertThat(item.quality()).isEqualTo(11);
    }

    @Test
    void increaseQuality_maxed() {
        var item = new DecoratedItem(anItem().quality(50).build());

        item.increaseQuality();

        assertThat(item.quality()).isEqualTo(50);
    }

    @Test
    void reduceToMinimumQuality() {
        var item = new DecoratedItem(anItem().quality(10).build());

        item.reduceToMinimumQuality();

        assertThat(item.quality()).isEqualTo(0);
    }

    @Test
    void progressSellIn() {
        var item = new DecoratedItem(anItem().sellIn(1).build());

        item.progressSellIn();

        assertThat(item.asItem()).is(withSellIn(0));
    }

    @Test
    void asItem() {
        Item value = anItem()
            .quality(ITEM_QUALITY)
            .name(ITEM_NAME)
            .sellIn(SELL_IN)
            .build();

        var actual = new DecoratedItem(value).asItem();

        assertThat(actual).is(withQuality(ITEM_QUALITY));
        assertThat(actual).is(withName(ITEM_NAME));
        assertThat(actual).is(withSellIn(SELL_IN));
    }

    @Test
    void sellByDateHasPassed() {
        assertThat(new DecoratedItem(anItem().sellIn(0).build()).sellByDateHasPassed()).isFalse();
        assertThat(new DecoratedItem(anItem().sellIn(-1).build()).sellByDateHasPassed()).isTrue();
    }

    @Test
    void sellInFiveDaysOrLess() {
        assertThat(new DecoratedItem(anItem().sellIn(6).build()).sellInFiveDaysOrLess()).isFalse();
        assertThat(new DecoratedItem(anItem().sellIn(5).build()).sellInFiveDaysOrLess()).isTrue();
        assertThat(new DecoratedItem(anItem().sellIn(4).build()).sellInFiveDaysOrLess()).isTrue();
    }

    @Test
    void sellInTenDaysOrLess() {
        assertThat(new DecoratedItem(anItem().sellIn(11).build()).sellInTenDaysOrLess()).isFalse();
        assertThat(new DecoratedItem(anItem().sellIn(10).build()).sellInTenDaysOrLess()).isTrue();
        assertThat(new DecoratedItem(anItem().sellIn(9).build()).sellInTenDaysOrLess()).isTrue();
    }

    @Test
    void progressWith() {
        var testableProgressStrategy = new TestableProgressRule();

        new DecoratedItem(anItem().build()).progressWith(testableProgressStrategy);

        assertThat(testableProgressStrategy.hasProgressed()).isTrue();
    }

    private static final class TestableProgressRule implements ProgressStrategy {

        private boolean progressed = false;

        @Override
        public DecoratedItem progress(DecoratedItem item) {
            this.progressed = true;
            return item;
        }

        public boolean hasProgressed() {
            return progressed;
        }
    }
}
