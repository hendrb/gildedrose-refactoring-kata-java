package com.gildedrose;

import org.approvaltests.Approvals;
import org.approvaltests.StoryBoard;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.gildedrose.Items.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void foo() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
    }

    @Test
    void viaGoldenMaster() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            new Item(DEXTERITY_VEST.asName(), 10, 20),
            new Item(AGED_BRIE.asName(), 2, 0),
            new Item(ELIXIR_OF_THE_MONGOOSE.asName(), 5, 7),
            new Item(SULFURAS.asName(), 0, 80),
            new Item(SULFURAS.asName(), -1, 80),
            new Item(BACKSTAGE_PASSES.asName(), 15, 20),
            new Item(BACKSTAGE_PASSES.asName(), 10, 49),
            new Item(BACKSTAGE_PASSES.asName(), 5, 49),
            new Item(CONJURED_MANA_CAKE.asName(), 4, 12)
        ));

        int days = 50;
        Approvals.verify(new StoryBoard()
            .add(app)
            .addFrames(days, app::progressOneDay)
        );
    }
}
