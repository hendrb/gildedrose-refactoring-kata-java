package com.gildedrose;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.gildedrose.ItemConditions.withQuality;
import static com.gildedrose.ItemConditions.withSellIn;
import static com.gildedrose.ItemFixture.anItem;
import static com.gildedrose.Items.*;
import static org.assertj.core.api.Assertions.assertThat;

class RulesTest {

    @Test
    void atTheEndOfEachDaySystemLowersQuality() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().quality(20).build()
        ));

        var actual = app.progressOneDay().items();

        assertThat(actual).first().is(withQuality(19));
    }

    @Test
    void atTheEndOfEachDaySystemLowersSellIn() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().sellIn(10).build()
        ));

        var actual = app.progressOneDay().items();

        assertThat(actual).first().is(withSellIn(9));
    }

    @Test
    void sellByDateExpires() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().sellIn(1).quality(12).build()
        ));

        var actual = app.progressDays(2).items();

        assertThat(actual).first().is(withSellIn(-1));
        assertThat(actual).first().is(withQuality(9));
    }

    @Test
    void sellByDateExpires_Bottom() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().sellIn(0).quality(1).build()
        ));

        var actual = app.progressDays(1).items();

        assertThat(actual).first().is(withSellIn(-1));
        assertThat(actual).first().is(withQuality(0));
    }

    @Test
    void qualityNeverNegative() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().quality(1).build()
        ));

        var actual = app.progressDays(3).items();

        assertThat(actual).first().is(withQuality(0));
    }

    @Test
    void agedBrieIncreasesInQuality() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(AGED_BRIE).quality(0).build()
        ));

        var actual = app.progressDays(5).items();

        assertThat(actual).first().is(withQuality(5));
    }

    @Test
    void qualityNeverMoreThan50() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().sellIn(10).name(AGED_BRIE).quality(49).build()
        ));

        var actual = app.progressDays(5).items();

        assertThat(actual).first().is(withQuality(50));
    }

    @Test
    void qualityNeverMoreThan50_SellInReached() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().sellIn(0).name(AGED_BRIE).quality(49).build()
        ));

        var actual = app.progressDays(1).items();

        assertThat(actual).first().is(withQuality(50));
        assertThat(actual).first().is(withSellIn(-1));
    }

    @Test
    void sulfurasLegendaryItem() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(SULFURAS).sellIn(0).quality(80).build(),
            anItem().name(SULFURAS).sellIn(-1).quality(80).build()
        ));

        var actual = app.progressDays(5).items();

        assertThat(actual).extracting(item -> item.quality).containsExactly(80, 80);
        assertThat(actual).extracting(item -> item.sellIn).containsExactly(0, -1);
    }

    @Test
    void backstagePassesIncreasesInQuality_tenDaysOrLess() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(BACKSTAGE_PASSES).quality(0).sellIn(11).build()
        ));

        var actual = app.progressDays(5).items();

        assertThat(actual).first().is(withQuality(9));
        assertThat(actual).first().is(withSellIn(6));
    }

    @Test
    void backstagePassesIncreasesInQuality_tenDaysOrLess_limit() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(BACKSTAGE_PASSES).quality(49).sellIn(11).build()
        ));

        var actual = app.progressDays(1).items();

        assertThat(actual).first().is(withQuality(50));
        assertThat(actual).first().is(withSellIn(10));
    }

    @Test
    void backstagePassesIncreasesInQuality_fiveDaysOrLess() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(BACKSTAGE_PASSES).quality(0).sellIn(6).build()
        ));

        var actual = app.progressDays(5).items();

        assertThat(actual).first().is(withQuality(14));
        assertThat(actual).first().is(withSellIn(1));
    }

    @Test
    void backstagePassesDropsToZeroAfterSellIn() {
        TestableGildedRose app = new TestableGildedRose(List.of(
            anItem().name(BACKSTAGE_PASSES).quality(45).sellIn(1).build()
        ));

        var actual = app.progressDays(2).items();

        assertThat(actual).first().is(withQuality(0));
        assertThat(actual).first().is(withSellIn(-1));
    }
}
