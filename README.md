What?
==
Having a Java language specific try at the [GildedRose-Refactoring-Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata#readme).

Prerequisites
==
* Java 10 (or later)

Build
==
    ./gradlew build
